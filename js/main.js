
// count initial ToDo
countTodos();

// capture click event
$("#checkAll").click(function(){
    AllDone();
});

//capture enter key press
$('.add-todo').on('keypress',function (e) {
      e.preventDefault // Do not submit form
      if (e.which == 13) { // check if enter is pressed
        var todo = $(".add-todo").val();
        addToDo(todo);
      }
});

// capture click event
$('#addTODO').on('click',function () {
    var todo = $(".add-todo").val();
    addToDo(todo);
});

// capute checkbox value change and transfer from ToDos to Already Done
$('.todolist').on('change','#sortable li input[type="checkbox"]',function(){
    if($(this).prop('checked')){
        var doneItem = $(this).parent().parent().find('label').text();
        $(this).parent().parent().parent().addClass('remove');
        done(doneItem);
        countTodos();
    }
});

// capture click event on button on Already Done
$('.todolist').on('click','.remove-item',function(){
    removeItem(this);
});

// add new todo
function addToDo(todo){
    createTodo(todo); 
    countTodos();
}

// count tasks
function countTodos(){
    var count = $("#sortable li").length;
    $('.count-todos').html(count);
}

//create task
function createTodo(text){
    var markup = '<li class="ui-state-default"><div class="checkbox"><label><input type="checkbox" value="" />'+ text +'</label></div></li>';
    $('#sortable').append(markup);
    $('.add-todo').val('');
}

//mark task as done
function done(doneItem){
    var done = doneItem;
    var markup = '<li>'+ done +'<button class="btn btn-default btn-xs pull-right  remove-item"><span class="fa fa-minus-square"></span></button></li>';
    $('#done-items').append(markup);
    $('.remove').remove();
}

//mark all tasks as done
function AllDone(){
    var myArray = [];

    $('#sortable li').each( function() {
         myArray.push($(this).text());   
    });
    
    // add to done
    for (i = 0; i < myArray.length; i++) {
        $('#done-items').append('<li>' + myArray[i] + '<button class="btn btn-default btn-xs pull-right  remove-item"><span class="glyphicon glyphicon-remove"></span></button></li>');
    }
    
    // myArray
    $('#sortable li').remove();
    countTodos();
}

//remove done task from list
function removeItem(element){
    $(element).parent().remove();
}